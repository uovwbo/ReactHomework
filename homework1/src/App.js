import React, { Component } from 'react';
import Guests from './guests.json';
import './main.css';



class ListItem extends Component {
  state = {
    done: true
  }
  done = (event) => {
    console.log( event, this.state )
    let status = this.state.done === true ? false : true;
    this.setState({ done: status });
  }
  render = () => {
    return(
      <div className={
        this.state.done === true ? "myList_Item" : "myList_Item done"
      }>
        <div>
          <p> Гость <span className="ourProps">{this.props.item.name}</span> работает в компании <span className="ourProps">{this.props.item.company}.</span></p>
          <p> Его контакти:</p>
          <p><span className="ourProps">{this.props.item.phone}</span>;</p>
          <p><span className="ourProps">{this.props.item.address}</span></p>
        </div>

        <div><button className="button_done" onClick={this.done}>Прибыл</button></div>
      </div>
    );
  }
};


class App extends Component {

  constructor(props){
    super(props);
    this.state = {
        myContacts: Guests,


    };
    this.someStuff = this.someStuff.bind(this);
  }
  someStuff = (event) => {
    console.log( event );

  }

  handleChange = (event) => {
    let searchQuery = event.target.value.toLowerCase();
    let renderContacts = Guests.filter( (item) => {
      let itemName = item.name.toLowerCase();
      return itemName.indexOf(searchQuery) !== -1;
    });
    this.setState({ myContacts: renderContacts});
    console.log( renderContacts );
  }

  render() {
    let { myContacts } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title" onClick={this.someStuff}>Список гостей</h1>
          <h2 className="App-title2">Список жертв</h2>
          <input className="MyInput" placeholder="Введите имя гостя для поиска" onChange={this.handleChange}/>
        </header>
        <div className="myList">
        {
          myContacts.length !== 0 ?
            myContacts.map( (item, key) => {
              return(
                <ListItem key={key} item={item} />

              );
            })
            :
            <div className="NotFind">We didn't find contacts from your search query!</div>

        }
        </div>
      </div>
    );
  }
}

export default App;
