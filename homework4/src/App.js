import React, { Component } from 'react';
import Home from './components/Home';
import About from './components/About';
import Artists from './components/Artists';
import Albums from './components/Albums';
import Contact from './components/Contact';
import NoFound from './components/NoFound';
import Helmet from 'react-helmet';

import './App.css';

import { Router, Route, Redirect, Switch, NavLink } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';


const supportsHistory = createBrowserHistory();

class App extends Component {
  render() {
    return (
        <div className="App">
            <Helmet>
                <html lang="en" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <title>Home Page</title>
            </Helmet>
            <Router history={supportsHistory}>
                <div>
                    <div className="nav">
                      <NavLink exact activeClassName="active" to="/">Home</NavLink>
                      <NavLink activeClassName="active" to="/about">About</NavLink>
                      <NavLink activeClassName="active" to="/artists">List</NavLink>
                      <NavLink activeClassName="active" to="/contact">Contact</NavLink>
                    </div>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route exact path="/about" component={About}/>
                        <Route exact path="/artists" component={Artists}/>
                        <Route exact path="/artists/album/:albumid" component={Albums}/>
                        <Route exact path="/contact" component={Contact}/>
                        <Route component={NoFound}/>

                    </Switch>

                </div>
            </Router>
        </div>

    );
  }
}

export default App;
