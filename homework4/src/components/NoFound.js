import React from 'react';

const NoFound = () => (<h1 className="noFound">Ops, Page not found!</h1>);

export default NoFound;
