import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Helmet from 'react-helmet';
import axios from 'axios';

class Artists extends Component {
    state = {
        loading: false,
        loaded: false,
        data: {}
    };

    componentDidMount() {
        let { match } = this.props;
        this.setState({
            loading: true
        });

        axios.post(
            `http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2`
        )
            .then( (response) => {
                console.log(response);
                this.setState({
                    data: response.data,
                    loading: false,
                    loaded: true
                })
            });

        
    }

    render() {
        let {data, loading, loaded} = this.state;
        if (loading === true) {
            return (
                <div>
                    <Helmet>
                        <html lang="en"/>
                        <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
                        <meta name="viewport" content="width=device-width, initial-scale=1"/>
                        <title>Artist List</title>
                    </Helmet>
                    <div className="wrap-page-content list-page">Page is loading</div>
                </div>

            )
        }
        if (loaded === true) {
            return (
                <div>
                    <Helmet>
                        <html lang="en"/>
                        <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
                        <meta name="viewport" content="width=device-width, initial-scale=1"/>
                        <title>Artist List</title>
                    </Helmet>
                    <div className="wrap-page-content list-page loaded">
                        {
                            data.map((artist, index) => (
                                <h2 className="artist-name" key={index}><Link
                                    to={`/artists/album/${artist.index}`}>{artist.name}</Link></h2>
                            ))
                        }

                    </div>
                </div>
            )
        }
        if (loaded === false && loading === false) {
            return (<div className="wrap-page-content list-page">Error</div>)
        }

    }
}

export default Artists;
