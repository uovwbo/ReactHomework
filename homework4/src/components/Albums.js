import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Albums extends Component{
    state = {
        loading: false,
        loaded: false,
        data: {}
    }

    componentDidMount() {
        let { match } = this.props;
        this.setState({
            loading: true
        })

        fetch(`http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2`)
            .then(res => res.json())
            .then(json => {
                console.log(json);
                this.setState({
                    data: json,
                    loading: false,
                    loaded: true
                });
                
            })
    }


    render(){
        let { data, loading, loaded } = this.state;
        if (loading === true) {
            return(<div className="wrap-page-content album-page">Page is loading</div>)
        }
        if (loaded === true) {
            return (
                <div className="wrap-page-content album-page loaded">
                {
                    data.map((artist) => {
                        return (
                            artist.album.map((albumName, index) => (
                                <h2 className="album-name" key={index}><Link
                                    to={`/artists/song/${index}`}>{albumName.name}</Link></h2>)

                            ))
                    })
                }

                </div>
            )
        }
        if( loaded === false && loading === false){
            return(<div className="wrap-page-content album-page">Error</div>)
        }

    }
}

export default Albums;
