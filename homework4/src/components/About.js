import React, { Component } from 'react';
import Helmet from 'react-helmet';

class About extends Component{
    render(){
        return(
            <div>
                <Helmet>
                    <html lang="en"/>
                    <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
                    <meta name="viewport" content="width=device-width, initial-scale=1"/>
                    <title>About</title>
                </Helmet>
                <div className="wrap-page-content">About</div>
            </div>
        )
    }
}

export default About;